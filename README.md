# [Utterly Experimental] Support for AutoDevOps Processing Multiple Containers in a Monorepo

### Background

AutoDevOps is built with the fundamental assumption that each repository only builds one Docker container. 

This is an attempt to make it build, deploy, update and operate on multiple containers in a monorepo using existing functionality and AutoDevOps customization as of GitLab version 13.2 (on GitLab.com)

This experiment includes understanding all the ways in which things do not work properly compared to the normal functional expectations of AutoDevOps building a single container.

### Architectural Heuristics Accomplished So Far

* AutoDevOps runs as a child pipeline for multiple containers configurations and keeps them disambiguated for:
  * Pipeline build logs and results
  * Container registry names
  * Environment names
  * Environment URLs
  * Container names
  * DNS and host names
* Dockerfiles internal references to external files (e.g. COPY) work relative to the Dockerfile location (the subdirectory)
* Subsequent builds of a branch appear to update the proper Kubernetes pods and do not cause new pods for each new update.
* **Serendipity**: Review environments build all the containers the first time they run.  For monorepos where the containers are actually a set of interacting microservices, this is actually **very desireable** since it ensures an **isolated full microservice environment**.  Review environments do follow "only:changes:" after the initial deployment of all containers when doing subsequent updates to the code.
* Dast environments and testing seem to be working.

### Design Choices (Hacks)

* **AUTO_DEVOPS_EXPLICITLY_ENABLED** had to be used.

* **CONTAINER_DIR** is specified in each container's "variables:" section in [.gitlab-ci.yml](./.gitlab-ci.yml) so that it flows, isolated, into the child pipeline for each container's AutoDevOps pipeline run.

* Adding CONTAINER_DIR to **CI_APPLICATION_REPOSITORY** is how the containers are disambiguated in the project's container registry.  This must be global so that all parts of autodevops use the path to build and retrieve the container.

* Adding CONTAINER_DIR to **HELM_RELEASE_NAME** is how Kubernetes Services are disambiguated (thanks to Hordur Freyr Yngvason for the guidance!).  This must be global.

* Adding CONTAINER_DIR to **CI_ENVIRONMENT_URL** is how the environment URL is disambiguated for each container / pod. This must be global

* .auto-deploy is customized to add CONTAINER_DIR to **CI_PROJECT_ID** so that the base DNS name for pods is disambiguated. This has been isolated to the auto-deploy job since it is not know what it might cause in other jobs.

* The overall defaults for autodevops can be set in [.gitlab-ci.yml](./.gitlab-ci.yml)

* The defaults for individual AutoDevOps phases can be set in [**common-overrides-for-autodevops.gitlab-ci.yml**](./common-overrides-for-autodevops.gitlab-ci.yml).

* Each container subdirectory has an include fragment for "trigger:include:" in [.gitlab-ci.yml](./.gitlab-ci.yml), these fragments could be used for customizing the code of jobs on a PER-CONTAINER + PER-JOB scope (without poluting the base .gitlab-ci.yml) - **so far this has not been needed, but the mechanism should be retained as this could end up being an important cusomization capability.**  

  Here are the examples:

  * [db-service/container-db-service.gitlab-ci.yml](./db-service/container-db-service.gitlab-ci.yml) 
  * [payment-service/container-db-service.gitlab-ci.yml](./payment-service/container-db-service.gitlab-ci.yml) 
  * [webui-service/container-db-service.gitlab-ci.yml](./webui-service/container-db-service.gitlab-ci.yml) 

### Limitations

* AutoDevOps before 13.2 requires a file named "Dockerfile" in the root of the repo or it refuses to run.  This example has a blank one.
* Containers and their Dockerfiles are stored in subdirectories, the subdirectory name must be set in a variable CONTAINER_DIR.
* AutoDevOps built-in sidecar PostgreSQL pods don't work well with this pattern and are disabled.
* The repository must have the setting "Skip outdated deployment jobs" disabled - which is different than the default.  Docs: docs: https://docs.gitlab.com/ee/ci/pipelines/settings.html#skip-outdated-deployment-jobs
* DOCKERFILE_PATH (new in 13.2 - thanks to Hordur Freyr Yngvason for making me aware of this!) is **not needed** because the build descends into each subdirectory during the build stage. This has the limitation and advantage that Dockerfile commands that reference local files (e.g. COPY) must use relative paths to the same directory where the Dockerfile lives.
* Some variables cannot be specified in gitlab-ci.yml "variables:" sections because they are not yet resolved.  These are then set in before_script items in [**common-overrides-for-autodevops.gitlab-ci.yml**](./common-overrides-for-autodevops.gitlab-ci.yml).  Some are scoped globally with "default:before_script:" while others are scoped with per-AutoDevOps job "before_script:"

### Known and Outstanding Issues (Discovered So Far)

* Security findings do get collected into the security console, but do no appear inline in Merge Requests.
* Variables that are specific to a container must appear under that containers trigger job in .gitlab-ci.yml so that they will be isolated when the child pipeline runs (do not declare variables for one specific container build as globals as they will apply to all container builds).
* Review environments links do not appear in the MRs (they can be retrieved from the individual job logs).
* Merging an MR only cleans up one of the containers at most - so "stop" must be clicked for each review environment within the pipeline where it was deployed so that all pods are shutdown.
* When building review environments, frequently one container get's the error `This job failed because the necessary resources were not successfully created.` - yet the job succeeds when reattempted.  The help page link provided for this error does not seem to list a feasible cause for this specific case of getting the error. This never seems to happen when building the default branch.
* Environment links from "environments" dashboards to not link to individual containers, but just one and it may be a dast environment.
* All scan / test jobs in AutoDevOps have not been tested for their results flowing back into the pipeline.  For example unit tests and Code Quality.

### Validations So Far

* Deploy to production environment pipeline: https://gitlab.com/guided-explorations/devops-patterns/multi-container-monorepo/-/pipelines/166397992
  * Production Environment:
    * db-service: http://19886987-production-db-service.eks.darwinjs.cloud
    * payment-service: http://19886987-production-payment-service.eks.darwinjs.cloud
    * webui-service: http://19886987-production-webui-service.eks.darwinjs.cloud
* MR, Pipeline and Review Environment for changing one container (payment-service):
  * MR: https://gitlab.com/guided-explorations/devops-patterns/multi-container-monorepo/-/merge_requests/6
  * Pipeline for initial change: https://gitlab.com/guided-explorations/devops-patterns/multi-container-monorepo/-/pipelines/166403652
  * Review Environment example URLs (**may already be shutdown**):
    * db-service: http://19886987-review-darwinjs-m-6uus2w-db-service.eks.darwinjs.cloud  (shutdown)
    * payment-service: http://19886987-review-darwinjs-m-6uus2w-payment-service.eks.darwinjs.cloud
    * webui-service: https://19886987-review-darwinjs-m-6uus2w-webui-service.eks.darwinjs.cloud
* Pipeline for update to v12 in MR (to test it only updates Kubernetes and does not create additional items): https://gitlab.com/guided-explorations/devops-patterns/multi-container-monorepo/-/pipelines/166433755

* Merge MR to master and deploy to production pipeline for changing one container: https://gitlab.com/guided-explorations/devops-patterns/multi-container-monorepo/-/pipelines/166439811

